$(document).ready(function() {
	var ctx = document.getElementById('trovit').getContext('2d');
	var myChart = new Chart(ctx, {
	  type: 'line',
	  data: {
			labels : [
				  "April","May","June",
				  "July","Agost","September",
				  "October","November","December","January","February","March"],
		datasets: [{
		  label: '€ / m2 Barcelona',
		 data : [4581.02,4603.03,4670.09,4637.59,4688.22,4625.02,4829.99,4988.50,5053.67,5131.11, 5299.02, 5503.44,6000, 0],
		  backgroundColor: "rgba(92,182,239,0.6)"
		}]
	  },
	  
	});
});